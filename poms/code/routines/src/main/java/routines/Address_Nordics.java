package routines;

public class Address_Nordics {

	private String externalID;
	private String country;
	private String name;
	private String zip;
	private String village;
	private String sub3;
	private String sub5;
	private String brick;
    
	public Address_Nordics(String externalID, String country, String name, String zip, String village, String sub3,String sub5, String brick){
    	this.externalID = externalID;
    	this.country = country;
    	this.name = name;
    	this.zip = zip;
    	this.village = village;
    	this.sub3 = sub3;
    	this.sub5 = sub5;
    	this.brick = brick;
    }
	
	public Address_Nordics(){
		this("", "", "", "", "", "", "","");
	}

	public String getExternalID() {
		return externalID;
	}

	public void setExternalID(String externalID) {
		this.externalID = externalID;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSub3() {
		return sub3;
	}

	public void setSub3(String sub3) {
		this.sub3 = sub3;
	}
	
	public String getSub5() {
		return sub5;
	}

	public void setSub5(String sub5) {
		this.sub5 = sub5;
	}

	public String getBrick() {
		return brick;
	}

	public void setBrick(String brick) {
		this.brick = brick;
	}

	
	
}
