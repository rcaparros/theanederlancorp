package routines;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.io.*; 

import routines.system.FastDateParser;
import routines.system.TalendTimestampWithTZ;

//import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

//import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class Formato {

    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static String formato_String(String str,Integer dig) {
    	if(str==null)str = "";
    	if(str.length()>dig) str = str.substring(0, dig);
    	
        while(str.length()<dig){
            str += " ";
        }
        return str;        
    }
    
   /* public synchronized static String formatDate(String pattern, java.util.Date date) {
        DateFormat format = FastDateParser.getInstance(pattern);
        if (date instanceof TalendTimestampWithTZ) {
            format.setTimeZone(((TalendTimestampWithTZ) date).getTimeZone());
        } else {
            format.setTimeZone(TimeZone.getDefault());
        }
        return format.format(date);
    }*/
    
    public static String formato_Fecha(Date fecha,Integer dig) {
    	
    	DateFormat df =	new SimpleDateFormat("yyyyMMdd");
    	String	str;
    	
    	if(fecha==null)	str = "";
    	else	str = df.format(fecha);
    	
        while(str.length()<dig){
            str += " ";
        }
        
        return str;        
    }
    












    public static String formato_Numero_Entero(Double num , Integer dig) {
    	String	str;
    	
    	str="";
    	
    	try{
    		if(num==null)str = "";
    		else{
		    	DecimalFormat fd = new DecimalFormat("#0"); 
		    	str = fd.format(num);
    		}
    	}catch (Exception ex) { 
    		 str="";
			 ex.printStackTrace();
			 System.out.println("-----------------------------------------Error-----------------------------------");
		} 
    
        while(str.length()<dig){
            str = "0" + str;
        }
        
        if(str.length()>dig) str = str.substring(0, dig);
        
        return str;     
    }
    public static String formato_Numero(Double num, Integer dig) {
    	String	str;
    	try{
    		if(num==null)str = "";
    		else{
		    	DecimalFormat fd = new DecimalFormat("#0.00"); 
		    	str = fd.format(num);
    		}
    	}catch (Exception ex) { 
    		 str="";
			 ex.printStackTrace();
			 System.out.println("-----------------------------------------Error-----------------------------------");
		} 
   
    	str = str.replace(".","");
    	str = str.replace(",","");
    	
        while(str.length()<dig){
            str = "0" + str;
        }
        
        if(str.length()>dig) str = str.substring(0, dig);
        
        return str;         
    }
    
    public static Date stringToDate(String strDate, String dateFormat) throws ParseException{
    	DateFormat df =	new SimpleDateFormat(dateFormat);
    	    	
    	if(strDate!=null && !strDate.isEmpty()){
    		strDate = strDate.replace("9999", "2099");
    		Date d1 = df.parse(strDate);
    		return d1;
    	}
    	else
    		return null;
    }
    
    public static void generar_fichero(List<List<String>> listaOL, List<List<String>> listaO, String ruta) {
    	
    	String userHomeFolder = System.getProperty("user.dir") + "/Files_BackUp"; 
    	//String userHomeFolder = "C:/"; 
    	System.out.println("***********\n " + ruta + "\n *********" + userHomeFolder);
    	
    	//Construyendo el archivo
    	String str="";
    	Date f = Calendar.getInstance().getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
    	String fecha = sdf.format(f);
    	if(listaO != null){
    		for(List<String> l :listaO){
    			for(String s:l){
    				str += s;
    			}
    			str += "\n";
    			for(List<String> m :listaOL){
    				if(m.get(1).contains(l.get(1))){
    	    			for(String n:m){
    	    				if(n.contains("***") == false)
    	    				str += n;
    	    			}
    	    			str += "\n";
    				}
    			}
    			str += "\n";
    		}
    		
    		// Guardando el archivo
    		
    		//File textFile = new File(userHomeFolder, "/Import/SUBSIDIARY/SOURCE/SPAIN/FLOW1/SPAIN_EXPORT_ORDERS_" + fecha + ".txt");
    		File directory = new File(userHomeFolder);
    	    if (! directory.exists())
    	        directory.mkdir();
    		File textFile = new File(userHomeFolder, "/SPAIN_EXPORT_ORDERS_" + fecha + ".txt");
    		FileWriter fw = null;
    		PrintWriter salArch = null;
    		try{
    		fw = new FileWriter(textFile); 
    		salArch = new PrintWriter(fw); 

    		salArch.print(str);
    		salArch.close(); 
    		}catch (IOException ex) { 
    			 ex.printStackTrace();
    			 System.out.println("-----------------------------------------Error-----------------------------------");
    		} 
    		System.out.println("***********\n " + str + "\n *********" );
    	}      
    	
    	
    	
    }
    
    public static void createFolder(String folderPath){
    	File directory = new File(folderPath);
	    if (! directory.exists())
	        directory.mkdir();
    }
    
    public static void generar_ficheros_lectura(List<List<String>> lista_clientesA, List<List<String>> lista_clientesB, List<List<String>> lista_clientesC) {
    	//Construyendo el archivo
    	String str="";
    	Date f = Calendar.getInstance().getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String fecha = sdf.format(f);
    	
    	for(Integer i = 0; i < lista_clientesA.size(); i++){
    		str += lista_clientesC.get(i).get(1) + "|" + lista_clientesA.get(i).get(1)+ "|" + lista_clientesA.get(i).get(2) + "|" + lista_clientesA.get(i).get(3) + "|" + lista_clientesA.get(i).get(4) + "|" + lista_clientesB.get(i).get(1) + "|" + lista_clientesB.get(i).get(3) + "|" + lista_clientesB.get(i).get(4);
    		str += "\n";
    	}
    		
    	// Guardando el archivo
    	String userHomeFolder = System.getProperty("user.dir") + "/Files_BackUp"; 
    	File textFile = new File(userHomeFolder, "/SPAIN_EXPORT_CUSTOMER_" + fecha + ".txt");
    		
    	FileWriter fw = null;
    	PrintWriter salArch = null;
    	try{
    		fw = new FileWriter(textFile); 
    		salArch = new PrintWriter(fw); 

    		salArch.print(str);
    		salArch.close(); 
    	}catch (IOException ex) { 
    		ex.printStackTrace();
    		System.out.println("-----------------------------------------Error-----------------------------------");
    	} 
    	System.out.println("***********\n " + str + "\n *********" );
    	      
    }
   
    public static void generar_ficheros_lectura_pedidos(List<List<String>> lista_clientesD) {
    	//Construyendo el archivo
    	String str="";
    	Date f = Calendar.getInstance().getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String fecha = sdf.format(f);
    	
    	for(Integer i = 0; i < lista_clientesD.size(); i++){
    		str += lista_clientesD.get(i).get(1) + "|" + lista_clientesD.get(i).get(2);
    		str += "\n";
    	}
    		
    	// Guardando el archivo
    	String userHomeFolder = System.getProperty("user.dir") + "/Files_BackUp"; 
    	File textFile = new File(userHomeFolder, "/SPAIN_EXPORT_ORDER_" + fecha + ".txt");
    		
    	FileWriter fw = null;
    	PrintWriter salArch = null;
    	try{
    		fw = new FileWriter(textFile); 
    		salArch = new PrintWriter(fw); 

    		salArch.print(str);
    		salArch.close(); 
    	}catch (IOException ex) { 
    		ex.printStackTrace();
    		System.out.println("-----------------------------------------Error-----------------------------------");
    	} 
    	System.out.println("***********\n " + str + "\n *********" );
    	      
    }
}
